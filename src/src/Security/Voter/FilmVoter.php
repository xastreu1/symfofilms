<?php

namespace App\Security\Voter;

use App\Entity\Film;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Security;

class FilmVoter extends Voter
{
    public function __construct(
        protected Security $security
    ){}

    protected function supports(string $attribute, $subject): bool
    {
        return in_array($attribute, ['edit', 'create', 'delete'])
            && $subject instanceof \App\Entity\Film;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        $method = 'can'.ucfirst($attribute);

        return $this->$method($subject, $user);
    }

    private function canEdit(Film $film, User $user): bool
    {
        return $user === $film->getCreatedBy() || $this->security->isGranted('ROLE_EDITOR');
    }

    private function canCreate(Film $film, User $user): bool
    {
        return true;
    }

    private function canDelete(Film $film, User $user): bool
    {
        return $this->canEdit($film, $user);
    }
}
