<?php

namespace App\Security\Voter;

use App\Entity\Actor;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Security;

class ActorVoter extends Voter
{
    public function __construct(
        protected Security $security
    ){}
    
    protected function supports(string $attribute, $subject): bool
    {
        return in_array($attribute, ['create', 'edit', 'delete'])
            && $subject instanceof \App\Entity\Actor;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        $method = 'can'.ucfirst($attribute);

        return $this->$method($subject, $user);
    }

    private function canEdit(Actor $actor, User $user): bool
    {
        return $user === $actor->getCreatedBy() || $this->security->isGranted('ROLE_EDITOR');
    }

    private function canCreate(Actor $actor, User $user): bool
    {
        return true;
    }

    private function canDelete(Actor $actor, User $user): bool
    {
        return $this->canEdit($actor, $user);
    }
}
