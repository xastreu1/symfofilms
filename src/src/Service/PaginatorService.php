<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;

class PaginatorService {

    private int $limit, $total, $page = 1;

    protected EntityManagerInterface $entityManager;

    public function __construct(int $limit = 10, EntityManagerInterface $entityManager)
    {
        $this->limit = $limit;
        $this->entityManager = $entityManager;
    }

    public function setLimit(int $limit): PaginatorService
    {
        $this->limit = $limit;
        return $this;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function getTotal()
    {
        return $this->total;
    }

    public function getTotalPages():int
    {
        return ceil($this->total / $this->limit);
    }

    public function paginate($sql, $page = 1): Paginator
    {
        $this->page = $page;
        $paginator = new Paginator($sql);
        $paginator->getQuery()
            ->setFirstResult($this->limit * ($page - 1))
            ->setMaxResults($this->limit);
        $this->total = $paginator->count();
        return $paginator;
    }
}