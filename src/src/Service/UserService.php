<?php

namespace App\Service;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class  UserService
{

    private EntityManager $entityManager;
    private UserRepository $userRepository;


    public function __construct(EntityManagerInterface $entityManager, UserRepository $userRepository)
    {
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
    }

    public function setUserRoles(User $user, array $roles):User
    {
        $user->setRoles($roles);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }

    public function getUserByEmail(string $email):User
    {
        return $this->userRepository->findOneBy(['email' => $email]);
    }

}
