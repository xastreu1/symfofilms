<?php

namespace App\Service;

use App\Entity\Actor;
use App\Entity\User;
use App\Form\ActorType;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

class  ActorService
{
    private FormFactory $formFactory;
    private FileUploaderService $fileUploaderService;
    private EntityManager $entityManager;

    private Form $form;

    public function __construct(
        FormFactoryInterface $formFactory,
        EntityManagerInterface $entityManager,
        FileUploaderService $fileUploaderService,
    ){
        $this->formFactory = $formFactory;
        $this->entityManager = $entityManager;
        $this->fileUploaderService = $fileUploaderService;
    }

    public function setPicture(Actor $actor, Form $form): Actor
    {
        $file = $form->get('picture')->getData();
        if ($file) {
            $actor->setPicture($this->fileUploaderService->replace($file, $actor->getPicture()));
        }
        return $actor;
    }

    public function createNewActor(Request $request, User $user): Actor
    {
        $actor = new Actor();
        $form = $this->createActorForm($request, $actor);

        if ($form->isSubmitted() && $form->isValid()) {
            $actor = $this->setPicture($actor, $form)
                    ->setCreatedBy($user);
            $this->entityManager->persist($actor);
            $this->entityManager->flush();
        }

        return $actor;
    }

    public function updateActor(Request $request, Actor $actor): Actor
    {
        $form = $this->createActorForm($request, $actor);
        $actor = $this->setPicture($actor, $form);
        $this->entityManager->flush();
        return $actor;
    }

    public function deleteActor(Actor $actor)
    {
        $this->entityManager->remove($actor);
        $this->entityManager->flush();
    }

    public function createActorForm(Request $request, Actor $actor): Form
    {
        $form = $this->formFactory->create(ActorType::class, $actor);
        $form->handleRequest($request);
        $this->form = $form;
        return $this->form;
    }

    public function getActorForm(): Form
    {
        return $this->form;
    }
}
