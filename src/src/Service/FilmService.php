<?php

namespace App\Service;

use App\Entity\Film;
use App\Entity\User;
use App\Form\FilmType;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

class  FilmService
{
    private FormFactory $formFactory;

    private EntityManager $entityManager;

    private FileUploaderService $fileUploaderService;

    private Form $form;

    public function __construct(FormFactoryInterface $formFactory, EntityManagerInterface $entityManager, FileUploaderService $fileUploaderService)
    {
        $this->formFactory = $formFactory;
        $this->entityManager = $entityManager;
        $this->fileUploaderService = $fileUploaderService;
    }

    public function saveFilm(Film $film)
    {
        $this->entityManager->persist($film);
        $this->entityManager->flush();
    }

    public function setPostedUrl(Film $film, Form $form): Film
    {
        $file = $form->get('poster_url')->getData();
        if ($file) {
            $film->setPosterUrl($this->fileUploaderService->replace($file, $film->getPosterUrl()));
        }
        return $film;
    }

    public function createNewFilm(Request $request, User $user): Film
    {
        $film = new Film();
        $form = $this->createFilmForm($request, $film);

        if ($form->isSubmitted() && $form->isValid()) {
            $film = $this->setPostedUrl($film, $form)
                        ->setCreatedBy($user);
            $this->saveFilm($film);
        }

        return $film;
    }

    public function updateFilm(Request $request, Film $film): Film
    {
        $form = $this->createFilmForm($request, $film);
        $this->setPostedUrl($film, $form);
        $this->entityManager->flush();
        return $film;
    }

    public function deleteFilm(Film $film)
    {
        $this->entityManager->remove($film);
        $this->entityManager->flush();
    }

    public function createFilmForm(Request $request, Film $film): Form
    {
        $form = $this->formFactory->create(FilmType::class, $film);
        $form->handleRequest($request);
        $this->form = $form;
        return $this->form;
    }

    public function getFilmForm(): Form
    {
        return $this->form;
    }
}
