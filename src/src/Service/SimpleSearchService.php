<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use App\Service\PaginatorService;

class SimpleSearchService
{
    public $field='id', $value='%', $order='id', $direction='DESC', $limit=5;

    public function __construct(
        protected EntityManagerInterface $entityManagerInterface,
        protected PaginatorService $paginatorService,
    ){}

    public function search(string $entityType, $formData)
    {
        $this->setFormData($formData);
        $query = $this->entityManagerInterface->createQuery(
            "SELECT p
            FROM $entityType p
            WHERE p.$this->field LIKE :value
            ORDER BY p.$this->order $this->direction"
        )->setParameter('value', "%$this->value%")
        ->setMaxResults($this->limit);
        return $query->getResult();
    }

    protected function setFormData($formData)
    {
        $this->field = $formData['field'];
        $this->value = $formData['value'];
        $this->order = $formData['order'];
        $this->direction = $formData['direction'];
        $this->limit = $formData['limit'];
    }
}