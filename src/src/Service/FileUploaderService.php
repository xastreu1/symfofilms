<?php

namespace App\Service;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploaderService
{
    protected string $targetDirectory;

    public function __construct(string $targetDirectory)
    {
        $this->targetDirectory = $targetDirectory;
    }

    public function upload(UploadedFile $file, bool $uniqueName = true)
    {
        $fileName = $uniqueName
            ? uniqid() . "." . $file->guessExtension()
            : $file->getClientOriginalName();

        try {
            $file->move($this->targetDirectory, $fileName);
        } catch (FileException $e) {
            return null;
        }

        return $fileName;
    }

    public function replace(UploadedFile $file, ?string $oldFile = null, bool $uniqueName = true)
    {
        $fileName = $uniqueName
            ? uniqid() . "." . $file->guessExtension()
            : $file->getClientOriginalName();
        
            try {
                $file->move($this->targetDirectory, $fileName);
                if ($oldFile) {
                    $fileSystem = new Filesystem();
                    $fileSystem->remove($this->targetDirectory . "/" . $oldFile);
                }
            } catch (FileException $e) {
                return null;
            }
    
            return $fileName;
    }
}