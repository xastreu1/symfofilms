<?php

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use App\Service\UserService;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;


#[AsCommand(
    name: 'app:users:set-rol',
    description: 'Set a rol for a user by email.',
)]
class UsersSetRolCommand extends Command
{

    public function __construct(
        protected UserService $userService
    ){
        parent::__construct();
    }


    protected function configure(): void
    {
        $this
            ->addArgument('user', InputArgument::REQUIRED, 'User email')
            ->addArgument('roles', InputArgument::REQUIRED, 'Roles to set')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $email = $input->getArgument('user');
        $roles = $input->getArgument('roles');

        if ($email && $roles) {
            $io->note(sprintf('You passed user: %s', $email));
            $io->note(sprintf('You passed roles: %s', $roles));
        } else {
            $io->error('Arguments user and roles are required');
            return Command::FAILURE;
        }

        $user = $this->userService->getUserByEmail($email);

        $this->userService->setUserRoles($user, [$roles]);

        $io->success('Roles setted!!');

        return Command::SUCCESS;
    }
}
