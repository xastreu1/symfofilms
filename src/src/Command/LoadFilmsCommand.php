<?php

namespace App\Command;

use App\Entity\Film;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Repository\FixturesRepository;
use Symfony\Component\HttpClient\HttpClient;
use Doctrine\ORM\EntityManagerInterface;

#[AsCommand(name: 'app:load-films', description: 'Insert into database films from an external source. Source: https://raw.githubusercontent.com/erik-sytnyk/movies-list/master/db.json')]
class LoadFilmsCommand extends Command
{
    protected EntityManagerInterface $entityManager;
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->entityManager = $em;
    }
    protected function configure(): void
    {
        $this
            ->addArgument('limit', InputArgument::OPTIONAL, 'Max of films to be created')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $limit = $input->getArgument('limit');
        $fixturesRepository = new FixturesRepository(HttpClient::create());

        if ($limit) {
            $io->note(sprintf('You passed a limit of films: %s films max', $limit));
            $io->title("$limit films will be created.");
        }

        $films = $fixturesRepository->fetchFilmsInformation();

        $count = 0;

        foreach ($films['movies'] as $film) {
            if ($limit && $limit <= $count) break;
            else $count++;
            $filmObject = new Film();
            $filmObject->setTitle($film['title']);
            $filmObject->setRunningTime($film['runtime']);
            $filmObject->setDirector($film['director']);
            $filmObject->setGenre($film['genres'][0]);
            $filmObject->setYear($film['year']);
            $filmObject->setActors($film['actors']);
            $filmObject->setPlot($film['plot']);
            $filmObject->setPosterUrl($film['posterUrl']);
            $this->entityManager->persist($filmObject);
        }

        $this->entityManager->flush();

        $io->success("Database filled with $count records!!!");

        return Command::SUCCESS;
    }
}
