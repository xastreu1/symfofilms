<?php

namespace App\Form;

use App\Entity\Actor;
use App\Repository\ActorRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddActorFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $ids = implode(",", $options['ids']);
        
        $builder
            ->add('actor', EntityType::class, [
                'class' => Actor::class,
                'choice_label' => 'name',
                'label' => 'Add Actor',
                'query_builder' => function (ActorRepository $actorRepository) use ($ids) {
                    return $actorRepository->createQueryBuilder('a')
                        ->where('a.id NOT IN (:ids)')
                        ->setParameter('ids', $ids)
                        ->orderBy('a.name', 'ASC');
                }
            ])
            ->add('Add', SubmitType::class, [
                'label' => 'Add',
                'attr' => [
                    'class' => 'btn btn-success my-3',
                ]
            ])
            ->setAction($options['action'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => null,
            'ids' => [],
        ]);
    }
}
