<?php

namespace App\Form;

use App\Entity\Film;
use App\Repository\FilmRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddFilmFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $ids = implode(",", $options['ids']);
        
        $builder
            ->add('film', EntityType::class, [
                'class' => Film::class,
                'choice_label' => 'title',
                'label' => 'Add Film',
                'query_builder' => function (FilmRepository $filmRepository) use ($ids) {
                    return $filmRepository->createQueryBuilder('f')
                        ->where('f.id NOT IN (:ids)')
                        ->setParameter('ids', $ids)
                        ->orderBy('f.title', 'ASC');
                }
            ])
            ->add('Add', SubmitType::class, [
                'label' => 'Add',
                'attr' => [
                    'class' => 'btn btn-success my-3',
                ]
            ])
            ->setAction($options['action'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => null,
            'ids' => [],
        ]);
    }
}
