<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchFilmType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('field', ChoiceType::class, [
                'choices' => $options['field_choices']
            ])
            ->add('value', TextType::class)
            ->add('order', ChoiceType::class, [
                'choices' => $options['order_choices']
            ])
            ->add('direction', ChoiceType::class, [
                'choices' => [
                    'Asc' => 'ASC',
                    'Desc' => 'DESC'
                ],
                'multiple' => false,
                'expanded' => true,
                'choice_attr' => [
                    'Asc' => ['checked' => 'checked'],
                ],
            ])
            ->add('limit', NumberType::class, [
                'required' =>  true,
                'html5' => true,
                'attr' => [
                    'min' => 1,
                    'max' => 25
                ]
            ])
            ->add('Search', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-primary'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'field_choices' => ['id' => 'Id'],
            'order_choices' => ['id' => 'Id'],
        ]);
    }
}
