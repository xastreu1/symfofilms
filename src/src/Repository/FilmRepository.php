<?php

namespace App\Repository;

use App\Entity\Film;
use App\Service\PaginatorService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Film|null find($id, $lockMode = null, $lockVersion = null)
 * @method Film|null findOneBy(array $criteria, array $orderBy = null)
 * @method Film[]    findAll()
 * @method Film[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FilmRepository extends ServiceEntityRepository
{
    protected PaginatorService $paginatorService;

    public function __construct(ManagerRegistry $registry, PaginatorService $paginatorService)
    {
        $this->paginatorService = $paginatorService;
        parent::__construct($registry, Film::class);
    }

    /**
     * @return integer
     *
    */
    public function countItems()
    {
        return $this->createQueryBuilder('u')
            ->select('count(u.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getPagination($link = 'film_index')
    {
        return [
            'page' => $this->paginatorService->getPage(),
            'total' => $this->paginatorService->getTotal(),
            'total_pages' => $this->paginatorService->getTotalPages(),
            'limit' => $this->paginatorService->getLimit(),
            'link' => $link
        ];
    }

    public function findAllPaginated($page)
    {
        $sql = $this->createQueryBuilder('u')->getQuery();
        return $this->paginatorService->paginate($sql, $page);
    }

    public function getLastFilms(int $max)
    {
        return $this->createQueryBuilder('u')
            ->setMaxResults($max)
            ->getQuery()
            ->getResult();
    }

}
