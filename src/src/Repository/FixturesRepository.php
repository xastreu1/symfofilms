<?php

namespace App\Repository;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class FixturesRepository
{
    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function fetchFilmsInformation(): array
    {
        $response = $this->client->request(
            'GET',
            'https://raw.githubusercontent.com/erik-sytnyk/movies-list/master/db.json'
        );
        $statusCode = $response->getStatusCode();
        //TO DO: check status code.
        $content = $response->toArray();
        return $content;
    }
}