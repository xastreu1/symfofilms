<?php

namespace App\Controller;

use App\Entity\Actor;
use App\Entity\Film;
use App\Form\AddActorFormType;
use App\Form\SearchFilmType;
use App\Repository\ActorRepository;
use App\Repository\FilmRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\FilmService;
use App\Service\SimpleSearchService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

#[Route('/film')]
class FilmController extends AbstractController
{

    #[Route('/list', name: 'film_index', methods: ['GET'])]
    public function index(FilmRepository $filmRepository, Request $request): Response
    {
        $page = $request->query->has('page') ? $request->query->get('page') : 1;
        return $this->render('film/index.html.twig', [
            'films' => $filmRepository->findAllPaginated($page),
            'pagination' => $filmRepository->getPagination(),
        ]);
    }

    #[Route('/show/{id}', name: 'film_show', methods: ['GET'])]
    public function show(Film $film): Response
    {
        return $this->render('film/show.html.twig', [
            'film' => $film,
        ]);
    }

    #[Route('/{id}/add/actor', name: 'film_add_actor', methods: ['POST'])]
    public function addActor(
        Request $request,
        Film $film,
        EntityManagerInterface $entityManager,
        LoggerInterface $appInfoLogger
    ): Response
    {
        $this->denyAccessUnlessGranted('edit', $film);
        $addActorForm = $this->createForm(AddActorFormType::class);
        $addActorForm->handleRequest($request);
        $actor = $addActorForm->getData()['actor'];
        $film->addActor($actor);
        $appInfoLogger->info($this->getUser()->getEmail() . " added actor " .$actor->getName(). " to film " . $film->getTitle());
        $entityManager->flush();
        $this->addFlash('success', 'The actor had been added.');
        return $this->redirectToRoute('film_edit', [
            'id' => $film->getId(),
        ]);
    }
    #[Route('/{id}/delete/actor/{id_actor}', name: 'film_delete_actor', methods: ['POST'])]
    public function deleteActor(
        Request $request,
        Film $film,
        EntityManagerInterface $entityManager,
        LoggerInterface $appInfoLogger,
        ActorRepository $actorRepository,
        $id_actor
    ): Response
    {
        $this->denyAccessUnlessGranted('edit', $film);
        $actor = $actorRepository->find($id_actor);
        $film->removeActor($actor);
        $appInfoLogger->info($this->getUser()->getEmail() . " added actor " .$actor->getName(). " to film " . $film->getTitle());
        $entityManager->flush();
        $this->addFlash('success', 'The actor had been deleted.');
        return $this->redirectToRoute('film_edit', [
            'id' => $film->getId(),
        ]);
    }


    #[Route('/search', name: 'film_search', methods: ['GET', 'POST'])]
    public function searchFilms(Request $request, FilmRepository $filmRepository, SimpleSearchService $simpleSearchService)
    {
        $form = $this->createForm(SearchFilmType::class, null, [
            'field_choices' => [
                'Title' => 'title',
                'Director' => 'director',
                'Genre' => 'genre'
            ],
            'order_choices' => [
                'Genre' => 'genre',
                'Running Time' => 'running_time',
                'Year' => 'year',
            ],
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $films = $simpleSearchService->search(Film::class, $data);
        } else {
            $films = $filmRepository->findAll();
        }

        return $this->render('film/film_search.html.twig', [
            'films' => $films,
            'search_form' => $form->createView(),
        ]);
    }

    #[Route('/new', name: 'film_new', methods: ['GET','POST'])]
    public function new(Request $request, FilmService $filmService, LoggerInterface $appInfoLogger): Response
    {
        $film = $filmService->createNewFilm($request, $this->getUser());

        $this->denyAccessUnlessGranted('create', $film);

        if ($film->getId()) {

            $this->addFlash(
                'success',
                "Film " . $film->getTitle() . " was created!"
            );
            $appInfoLogger->info($this->getUser()->getEmail() . " create film " . $film->getTitle());
            return $this->redirectToRoute('film_show', [
                'id' => $film->getId(),
            ], Response::HTTP_SEE_OTHER);

        }

        return $this->renderForm('film/new.html.twig', [
            'film' => $film,
            'form' => $filmService->getFilmForm()
        ]);
    }

    #[Route('/{id}/edit', name: 'film_edit', methods: ['GET','POST'])]
    public function edit(Request $request, Film $film, FilmService $filmService, LoggerInterface $appInfoLogger): Response
    {
        $this->denyAccessUnlessGranted('edit', $film);
        $form = $filmService->createFilmForm($request, $film);

        $ids = [];
        foreach ($film->getActors() as $actor) {
            $ids[] = $actor->getId();
        }

        $addActorForm = $this->createForm(AddActorFormType::class, null, [
                'action' => $this->generateUrl('film_add_actor', [
                            'id' => $film->getId(),
                ]),
                'ids' => $ids,
            ]);

        if ($form->isSubmitted() && $form->isValid()) {
            $film = $filmService->updateFilm($request, $film);
            $appInfoLogger->info($this->getUser()->getEmail() . " edit film " . $film->getTitle());
            return $this->redirectToRoute('film_show', [
                'id' => $film->getId(),
            ], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('film/edit.html.twig', [
            'film' => $film,
            'addActorForm' => $addActorForm,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'film_delete', methods: ['POST'])]
    public function delete(Request $request, Film $film, FilmService $filmService, LoggerInterface $appInfoLogger): Response
    {
        $this->denyAccessUnlessGranted('delete', $film);
        if ($this->isCsrfTokenValid('delete'.$film->getId(), $request->request->get('_token'))) {
            $appInfoLogger->info($this->getUser()->getEmail() . " delete film " . $film->getTitle());
            $filmService->deleteFilm($film);
        }

        return $this->redirectToRoute('film_index', [], Response::HTTP_SEE_OTHER);
    }
}
