<?php

namespace App\Controller;

use App\Form\ContactFormType;
use App\Repository\ActorRepository;
use App\Repository\FilmRepository;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(ActorRepository $actorRepository, FilmRepository $filmRepository): Response
    {
        
        return $this->render('home/index.html.twig', [
            'actors' => $actorRepository->countItems(),
            'films' => $filmRepository->countItems(),
            'last_films' => $filmRepository->getLastFilms(9)
        ]);
    }

    #[Route('/contact', name: 'contact_form', methods: ['GET','POST'])]
    public function contact(Request $request, MailerInterface $mailer)
    {
        $form = $this->createForm(ContactFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            dump($data);
            $email = (new TemplatedEmail())
                ->from('bcnspeeddating@gmail.com')
                ->to('arodriguez@leadwizzer.com')
                ->subject($data['subject'])
                ->htmlTemplate('home/email-template.html.twig')
                ->context([
                    'message' => $data['message'],
                ]);
            $mailer->send($email);
            $this->addFlash('success', "The email was sent correctly");
            return $this->redirectToRoute('home');
        }

        return $this->renderForm('home/contact.html.twig',[
            'form' => $form,
        ]);

    }
}
