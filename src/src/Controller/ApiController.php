<?php

namespace App\Controller;

use App\Entity\Film;
use App\Repository\ActorRepository;
use App\Repository\FilmRepository;
use App\Service\FilmService;
use stdClass;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;

#[Route('/api')]
class ApiController extends AbstractController
{

    protected Serializer $serializer;

    public function __construct()
    {
        $this->serializer = new Serializer([
            new ObjectNormalizer()
        ],[
            new JsonEncoder(), new XmlEncoder(), new CsvEncoder()
        ]);
    }

    #[Route('/film/create/{format}', methods: ['POST'], requirements: ['format' => 'json|xml|csv'], defaults: ['format' => 'json'] )]
    public function createFilm(Request $request, FilmService $filmService, $format): Response
    {
        try {
            $film = $this->serializer->deserialize(
                $request->getContent(),
                Film::class,
                $format,
            );
        } catch (NotEncodableValueException $e) {
            $response = new stdClass();
            $response->status = "Proccessing error, invalid format";
            return new JsonResponse($response, JsonResponse::HTTP_BAD_REQUEST);
        }
        $filmService->saveFilm($film);

        $serialized = $this->serializer->serialize($film, $format);
        
        return $this->getFormatedResponse($serialized, $format);
    }


    #[Route('/films/list/{format}', name: 'api_films_list', requirements: ['format' => 'json|xml|csv'], defaults: ['format' => 'json'] )]
    public function listFilms(FilmRepository $filmRepository, $format): Response
    {

        $films = $filmRepository->findAll();

        $serialized = $this->serializer->serialize($films, $format);
        
        return $this->getFormatedResponse($serialized, $format);
    }

    #[Route('/actors/list/{format}', name: 'api_actors_list', requirements: ['format' => 'json|xml|csv'], defaults: ['format' => 'json'] )]
    public function listActors(ActorRepository $actorRepository, $format): Response
    {
        
        $actors = $actorRepository->findAll();

        $serialized = $this->serializer->serialize($actors, $format);
        
        return $this->getFormatedResponse($serialized, $format);
    }

    protected function getFormatedResponse($data, $format)
    {
        $response = new Response($data);
        switch ($format) {
            case 'json':
                $format = 'application/json';
                break;
            case 'json':
                $format = 'text/xml';
                break;
            case 'json':
                $format = 'text/csv';
                break;
        }
        $response->headers->set('Content-Type', $format);
        return $response;
    }
}
