<?php

namespace App\Controller;

use App\Entity\Actor;
use App\Repository\ActorRepository;
use App\Service\ActorService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\AddFilmFormType;
use Psr\Log\LoggerInterface;

#[Route('/actor')]
class ActorController extends AbstractController
{
    #[Route('/list', name: 'actor_index', methods: ['GET'])]
    public function index(ActorRepository $actorRepository): Response
    {
        return $this->render('actor/index.html.twig', [
            'actors' => $actorRepository->findAll(),
        ]);
    }

    #[Route('/{id}/add/film', name: 'actor_add_film', methods: ['POST'])]
    public function addFilm(
        Request $request,
        Actor $actor,
        EntityManagerInterface $entityManager,
        LoggerInterface $appInfoLogger
    ): Response
    {
        $this->denyAccessUnlessGranted('edit', $actor);
        $addFilmForm = $this->createForm(AddFilmFormType::class);
        $addFilmForm->handleRequest($request);
        $film = $addFilmForm->getData()['film'];
        $actor->addFilm($film);
        $appInfoLogger->info($this->getUser()->getEmail() . " add film ". $film->getTitle() ." to actor " . $actor->getName());
        $entityManager->flush();
        $this->addFlash('success', 'The film had been added.');
        return $this->redirectToRoute('actor_edit', [
            'id' => $actor->getId(),
        ]);
    }

    #[Route('/new', name: 'actor_new', methods: ['GET','POST'])]
    public function new(Request $request, ActorService $actorService, LoggerInterface $appInfoLogger): Response
    {
        $actor = $actorService->createNewActor($request, $this->getUser());
        $this->denyAccessUnlessGranted('create', $actor);

        if ($actor->getId()) {
            $appInfoLogger->info($this->getUser()->getEmail() . " create actor " . $actor->getName());
            $this->addFlash(
                'success',
                "Actor " . $actor->getName() . " was created!"
            );

            return $this->redirectToRoute('actor_show', [
                'id' => $actor->getId(),
            ], Response::HTTP_SEE_OTHER);

        }

        return $this->renderForm('actor/new.html.twig', [
            'actor' => $actor,
            'form' => $actorService->getActorForm(),
        ]);
    }

    #[Route('/show/{id}', name: 'actor_show', methods: ['GET'])]
    public function show(Actor $actor): Response
    {
        return $this->render('actor/show.html.twig', [
            'actor' => $actor,
        ]);
    }

    #[Route('/{id}/edit', name: 'actor_edit', methods: ['GET','POST'])]
    public function edit(Request $request, Actor $actor, ActorService $actorService, LoggerInterface $appInfoLogger): Response
    {
        $this->denyAccessUnlessGranted('edit', $actor);
        $form = $actorService->createActorForm($request, $actor);

        $ids = [];
        foreach ($actor->getFilms() as $film) {
            $ids[] = $film->getId();
        }

        $addFilmForm = $this->createForm(AddFilmFormType::class, null, [
                'action' => $this->generateUrl('actor_add_film', [
                            'id' => $actor->getId(),
                ]),
                'ids' => $ids,
            ]);

        if ($form->isSubmitted() && $form->isValid()) {
            $actor = $actorService->updateActor($request, $actor);
            $appInfoLogger->info($this->getUser()->getEmail() . " edit actor " . $actor->getName());
            $this->addFlash(
                'success',
                "Actor " . $actor->getName() . " was updated!"
            );
            return $this->redirectToRoute('actor_show', [
                'id' => $actor->getId(),
            ], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('actor/edit.html.twig', [
            'actor' => $actor,
            'addFilmForm' => $addFilmForm,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'actor_delete', methods: ['POST'])]
    public function delete(Request $request, Actor $actor, ActorService $actorService, LoggerInterface $appInfoLogger): Response
    {
        $this->denyAccessUnlessGranted('delete', $actor);
        if ($this->isCsrfTokenValid('delete'.$actor->getId(), $request->request->get('_token'))) {
            $this->addFlash(
                'danger',
                "Actor " . $actor->getName() . " was deleted!"
            );
            $actorService->deleteActor($actor);
            $appInfoLogger->info($this->getUser()->getEmail() . " delete actor " . $actor->getName());
        }
        return $this->redirectToRoute('actor_index', [], Response::HTTP_SEE_OTHER);
    }
}
