FROM php:8-fpm-alpine
#Configure intl
RUN apk add icu-dev 
RUN docker-php-ext-configure intl
RUN docker-php-ext-install intl
# Install Mysql PDO
RUN docker-php-ext-install pdo pdo_mysql
# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
# Install NPM
RUN apk add --update npm
# Install Symfony CLI
# RUN curl -sS https://get.symfony.com/cli/installer | bash
# RUN mv /root/.symfony/bin/symfony /usr/local/bin/symfony